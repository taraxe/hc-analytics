import sbt._
import Keys._
import PlayProject._

object ApplicationBuild extends Build {

    val appName         = "application"
    val appVersion      = "1.0"

    val appDependencies = Seq(
    )

    val main = PlayProject(appName, appVersion, appDependencies).settings(defaultScalaSettings:_*).settings(
    )

}
