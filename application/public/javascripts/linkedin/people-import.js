/**
 * User: alabbe
 * Date: 16/09/11
 * Time: 14:09
 */

$(document).ready(function() {
    var employees = [];
	var company = $('#company-name').val();

	$('.pagination li a').click(function () {
		getPage($(this).text() - 1);
	});

    $('form').submit(function(){
        $.ajax({
            url : jsRoutes.controllers.Application.employees(company).url,
            data: JSON.stringify({employees : employees,
                    company: company}),
            type : 'post',
            contentType: 'application/json'
        })
            .done(function(r){
                console.log(r);
                window.location = jsRoutes.controllers.Application.report(company,r).url;
            })
            .fail(function(a,b,c){
                console.log(a + " "+ b +" "+c);
            })
        return false;
    });


	function getPage(n) {
		$.ajax({
			url: jsRoutes.controllers.LinkedIn.employees(company,n),
			dataType: 'json',
			error : function (xhr, status) {
				console.error(xhr.errorCode + ' ' + status);
			},
			success: function (data) {
				console.debug('people retrieved retrieved');
			}
		})
	}

	$('#select-all-checkbox').change(function() {
		var changeObject = {};
		if (this.checked) {
			changeObject["action"] = "add"
		} else {
			changeObject["action"] = "remove"
		}
		var values = [];

		$('.select-checkbox').filter(
		  function() {
			  return !$(this).attr('disabled')
		  }).each(function() {
			  changeObject.action == "add" ? $(this).attr('checked', 'checked') : $(this).removeAttr('checked');
			  values.push($(this).val());
		  });
		changeObject["values"] = values;
		updateHiddenFields(changeObject)
	});

	$('.select-checkbox').change(function() {
		var changeObject = {};
		if (this.checked) {
			changeObject["action"] = "add"
		} else {
			changeObject["action"] = "remove"
		}
		changeObject["values"] = [$(this).val()];

		updateHiddenFields(changeObject);
	});

	function updateHiddenFields(changeObject) {
		if (changeObject.action == "add") {
			$.each(changeObject.values, function(i, e) {
				if ($.inArray(e, employees) == -1) {
					employees.push(e);
				}
			});
		}
		else {
			employees = $.grep(employees, function(e) {
				return $.inArray(e, changeObject.values) == -1;
			});
		}
	};




});

