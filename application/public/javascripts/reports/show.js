/**
 * User: alabbe
 * Date: 16/09/11
 * Time: 14:09
 */

$(document).ready(function() {
	var width = 640;
	var height = 600;
	var scale;
	var detailsContainer = $('#details-container');
	var metaContainer = $('#metadata-container');
	var chart = $('#chart');
	var reportId = chart.data('id');

	var circleStroke = 20;   // should be 2.5 (eg the stroke width), but width of the svg container is not well calculated by webkit...

	$.ajax({
		url: jsRoutes.controllers.Report.index(reportId).url,
		dataType: 'json'
		/*data: {"w": width, "h" : height},*/
    })
        .fail(function (xhr, status) {
			console.error(xhr.errorCode + ' ' + status);
    })
        .done(function (data) {
			console.debug('graph retrieved');
			chart.empty();
			preProcessData(data);
			buildLayout(data);
		});


	var preProcessData = function(data) {
		var nodeLookup = {};
		$.each(data.nodes, function(i, n) {
			nodeLookup[n.label] = i;
			n["fixed"] = true;
		});

		$.each(data.edges, function(i, n) {
			n.source = nodeLookup[n.source];
			n.target = nodeLookup[n.target];
		});

		scaleFn(data.nodes);
	};

	var scaleFn = function(nodes) {
		var minX = Number.MAX_VALUE;
		var maxX = -Number.MAX_VALUE;
		var minY = Number.MAX_VALUE;
		var maxY = -Number.MAX_VALUE;

		$.each(nodes, function(i, n) {
			n.x *= width;
			n.y *= height;

			var r = rayon(n.weight);

			minX = Math.min(minX, n.x - r - circleStroke);
			maxX = Math.max(maxX, n.x + r + circleStroke);
			minY = Math.min(minY, n.y - r - circleStroke);
			maxY = Math.max(maxY, n.y + r + circleStroke);
		});

		scale = Math.min(width / (maxX - minX), height / (maxY - minY));

		$.each(nodes, function(i, n) {
			n.x = (n.x - minX) * scale;
			n.y = (n.y - minY) * scale;
		});

	};

	var rayon = function(weight) {
		return 2 * weight;
	};

	var buildLayout = function(data) {
		var fill = d3.scale.category10();
//todo make the scaleFn match a correct ordinal scaleFn

		var vis = d3.select('#chart')
		  .append('svg:svg')
		  .attr('width', width)
		  .attr('height', height);

		var force = d3.layout.force()
		  .nodes(data.nodes)
		  .links(data.edges)
		  .size([width, height])
		  .start();

		var link = vis.selectAll("line.edge")
		  .data(data.edges)
		  .enter().append("svg:line")
		  .attr("class", "edge")
		  .style("stroke-width", function(d) {
			  return Math.sqrt(d.length);
		  })
		  .attr("x1", function(d) {
			  return d.source.x;
		  })
		  .attr("y1", function(d) {
			  return d.source.y;
		  })
		  .attr("x2", function(d) {
			  return d.target.x;
		  })
		  .attr("y2", function(d) {
			  return d.target.y;
		  });

		var node = vis.selectAll("circle.node")
		  .data(data.nodes)
		  .enter().append("svg:circle")
		  .attr("class", "node")
		  .attr("cx", function(d) {
			  return d.x;
		  })
		  .attr("cy", function(d) {
			  return d.y;
		  })
		  .attr("r", function(d) {
//return 23 * (1 / d.weight) + 2;
			  return rayon(d.weight) * scale;
		  })
		  .style("fill", function(d) {
			  return fill(d.anchor ? 0 : 1);
		  })

			/*.call(force.drag)*/;

		node.append("svg:title")
		  .text(function(d) {
			  return d.label;
		  });

		/*text display*/
		/*vis.selectAll('text').data(data.nodes).enter().append('svg:text')
		 .text(function(d) {
		 return d.label;
		 })
		 .attr('x',function(d){
		 return d.x;
		 })
		 .attr('y',function(d){
		 return d.y;
		 });*/

		vis.style("opacity", 1e-6)
		  .transition()
		  .duration(500)
		  .style("opacity", 1);

		force.stop();


		d3.selectAll("circle.node").on("click", function() {

// remove previously selected
			$('circle.node.selected').each(function(i, e) {
				var s = $(e).attr('class');
				$(e).attr('class', s.replace(' selected', ''));
			});

// select the one clicked
			$(this).attr('class', $(this).attr('class') + ' selected');

// clear the detail container
			detailsContainer.empty();

// add detailsContainer
			var detail = $('<ul class="unstyled"/>')
			$.each(this.__data__, function(i, e) {
				detail.append('<li>' + i + ' : ' + e + '</li>');
			});
			detailsContainer.append(detail);

		});
	};



	/*$('circle.node').twipsy({
		'title': function() {
			return this['__data__'].label
		},
		'top': function() {
			return this.cx;
		},
		'left': function() {
			return this.cy;
		}

	})*/
});

