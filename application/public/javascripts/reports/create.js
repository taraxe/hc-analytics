/**
 * User: alabbe
 * Date: 16/09/11
 * Time: 14:09
 */

/*$(document).ready(function() {

 });*/

$(function() {
	function split(val) {
		return val.split(/,\s*/);
	}

	function extractLast(term) {
		return split(term).pop();
	}

	$("#auto-complete")
	  .bind("keydown", function(event) {
		  if (event.keyCode === $.ui.keyCode.TAB &&
			$(this).data("autocomplete").menu.active) {
			  event.preventDefault();
		  }
	  })
	  .autocomplete({
		  source: function(request, response) {
			  $.ajax({
				  url: encodeURI(dataUrl+'?query='+extractLast(request.term)),
				  dataType: "json",
				  method: "get",
				  /*data: {
					  'q': extractLast(request.term)
				  },*/
				  success: function(data) {
					  response($.map(data, function(item) {
						  return {
							  label: item.firstName + " " + item.lastName  + (item.email ? ' (' + item.email + ')' : ''),
							  value: item.userName
						  }
					  }));
				  }
			  });
		  },
		  minLength: 2,
		  focus: function() {
			  // prevent value inserted on focus
			  return false;
		  },
		  select: function(event, ui) {
			  var terms = split(this.value);
			  // remove the current input
			  terms.pop();
			  if ($.inArray(ui.item.value, terms) == -1) {

				  // add the selected item
				  terms.push(ui.item.value);
				  // add placeholder to get the comma-and-space at the end

			  }
			  terms.push("");
			  this.value = terms.join(", ");
			  return false;

		  }/*,
		   open: function() {
		   $( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
		   },
		   close: function() {
		   $( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
		   }*/
	  });
});

