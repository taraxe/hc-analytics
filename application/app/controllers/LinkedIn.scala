package controllers

import play.api.mvc._
import play.api._
import play.api.libs._
import play.api.libs.oauth._
import play.api.libs.json._
import play.api.libs.json.Json._
import play.api.libs.ws._
import play.api.http.ContentTypes
import play.api.i18n.Messages
import play.api.libs.concurrent._
import play.api.data.validation._

import models._
import models.LinkedinProtocol._
import views._

/**
 * Created by alabbe
 * Date: 21/10/11
 * Time: 17:55
 */


object LinkedIn extends Controller /*with Secured*/ {
  def urlencode(s: String) = java.net.URLEncoder.encode(s, "UTF-8")

  val SEARCH_COMPANY_URL = (name: String) =>
    "http://api.linkedin.com/v1/company-search:(companies:(id,name))?format=json&keywords=" + urlencode(name)
  val SEARCH_PEOPLE_URL = "http://api.linkedin.com/v1/people-search:(id,first-name,last-name,skills)?company-name={company-name}&current-company=true"
  val COMPANY_URL = "http://api.linkedin.com/v1/companies?format=json&email-domain="
  val PEOPLE_SEARCH_URL = (n: Int, p: Int, name: String) =>
    "http://api.linkedin.com/v1/people-search?format=json&current-company=true&count=%d&start=%d&company-name=%s".format(n, p, urlencode(name))
  val SKILLS_SEARCH_URL = "http://api.linkedin.com/v1/people::({userIds}):(id,first-name,last-name,skills)?format=json" // id=12345


  def companies(term: String) = Action { implicit request =>
    AsyncResult(WS.url(LinkedIn.SEARCH_COMPANY_URL(term))
      .sign(OAuthCalculator(Authentication.LINKEDIN_KEY, Authentication.tokenPair))
      .get()
      .extend {
        _.value match {
          case Redeemed(response) => Ok(response.body).as(ContentTypes.JSON)
          case Thrown(t) => throw t
        }
      })
  }

  def company(companyName: String)(implicit request: RequestHeader): Promise[Option[Company]] = {
    WS.url(LinkedIn.COMPANY_URL + companyName)
      .sign(OAuthCalculator(Authentication.LINKEDIN_KEY, Authentication.tokenPair))
      .get()
      .map(response => response.json.asOpt[CompanyList].flatMap(_.first))
  }

  /**
   * n is the number of findEmployees requested
   * p is the page number
   * company is the name of the company
   */
  def findEmployees(company: String, n: Int = 10, p: Int = 1)(implicit request: RequestHeader): Promise[Option[PeopleSearchResult]] = {
     Logger.debug(LinkedIn.PEOPLE_SEARCH_URL(n, p, company))
     WS.url(LinkedIn.PEOPLE_SEARCH_URL(n, p, company))
      .sign(OAuthCalculator(Authentication.LINKEDIN_KEY, Authentication.tokenPair))
      .get().map { response => response.json.asOpt[PeopleSearchResult] }
  }
   
  def employees(company:String,  n:Int = 10, p:Int =1 ) = Action { implicit request =>
     AsyncResult(findEmployees(company,n,p) map  {
        case Some(e) => Ok(toJson[PeopleSearchResult](e)).as(ContentTypes.JSON)
        case None => NotFound
     })
  }   

  def skills(company:String,  people: List[String])(implicit request: RequestHeader) : Promise[Option[PeopleSkills]] = {
    WS.url(LinkedIn.SKILLS_SEARCH_URL.replace("{userIds}", people.mkString(",")))
      .sign(OAuthCalculator(Authentication.LINKEDIN_KEY, Authentication.tokenPair))
      .get().map {response => {
       Logger.debug(response.json.toString())
       response.json.asOpt[PeopleSkills]}
    }
   }
}

