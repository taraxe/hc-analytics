package controllers

import play.api._
import mvc.Action._
import play.api.mvc._

import models._
import utils.FileUtils
import views._
import play.api.libs.concurrent._
import play.api.libs.json._
import play.api.libs.concurrent.Akka._
import LinkedinProtocol._
import java.io.{FileNotFoundException, File}
import akka.actor._
import akka.util.Timeout
import akka.util.duration._
import models.LayoutWorker.{Layout, LinLog}

object Application extends Controller {


  /* Render a form for entering a company name */
  def index = Action { implicit request =>
                  Ok(html.index())
              }

  /* Show a list of employees of the given company */
  def company(name: String) = Action { implicit request =>
     AsyncResult(LinkedIn.findEmployees(name).map {
        _.toRight(BadRequest)
         .right.map{ employees => Ok(html.linkedin.employees(employees.people, name))}.fold(identity,identity)
     })
  }

   def employees(company:String) = Action(parse.json) { implicit request =>
      val skillsOrBadRequest =
            (request.body \ "employees")
                  .asOpt[List[String]]
                  .toRight(BadRequest("Invalid json"))
      skillsOrBadRequest.right.map { employees =>
         AsyncResult(
            LinkedIn.skills(company, employees).map( _.map(x => Ok(models.Report(x).id)).getOrElse(Ok("no"))))

      }.fold(identity,identity)
   }

   def report(name:String, id:String) = Action { implicit request =>
      try {
         new File(FileUtils.appDir.getAbsolutePath + File.separator + id)
         Ok(html.linkedin.report(name,id))
      } catch {
         case e:FileNotFoundException => NotFound(e.getMessage)
      }
   }
  

   // -- Javascript routing

   def javascriptRoutes = Action {
      import routes.javascript._
      Ok(
         Routes.javascriptRouter("jsRoutes")(
            routes.javascript.Application.company,
            routes.javascript.LinkedIn.companies,
            routes.javascript.LinkedIn.employees,
            routes.javascript.Application.employees,
            routes.javascript.Application.report,
            routes.javascript.Report.index
         )
      ).as("text/javascript")
   }
}

trait Secured {
  
  /**
   * Retrieve the connected user email.
   */
  private def username(request: RequestHeader) = request.session.get("email")

  /**
   * Redirect to login if the user in not authorized.
   */
  private def onUnauthorized(request: RequestHeader) = Results.Forbidden
  
  // --
  
  /** 
   * Action for authenticated users.
   */
  def IsAuthenticated(f: => String => Request[AnyContent] => Result) = Security.Authenticated(username, onUnauthorized) { user =>
    Action(request => f(user)(request))
  }
}
