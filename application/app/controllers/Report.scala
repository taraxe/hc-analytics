package controllers

import play.api._
import play.api.mvc._

import models._
import utils.FileUtils
import views._
import play.api.libs.concurrent._
import akka.dispatch.{ Future, Await }
import akka.actor.ActorSystem
import java.io.{FileNotFoundException, File}
import akka.actor._
import akka.util.duration._
import models.LayoutWorker.{Layout, LinLog}

object Report extends Controller {

   def index(id:String) = Action { implicit request =>
      try {
         val file = new File(FileUtils.appDir.getAbsolutePath + File.separator + id)
         val layoutFuture = LayoutWorker.ref ? (LinLog(file.getAbsolutePath),timeout = 10.second)

         //todo how to convert Future to AsyncResult
         AsyncResult { layoutFuture.mapTo[Layout].asPromise.map{ l =>
            Ok(l.json)
         }}
         //Ok(toJson(new AnchoredLinLogLayout(file.getAbsolutePath).execute(100)))
      } catch {
         case e:FileNotFoundException => NotFound("report does not exist")
      }
   }
}


