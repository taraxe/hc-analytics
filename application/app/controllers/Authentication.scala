package controllers

import play.api.mvc._
import play.api._
import play.api.libs._
import play.api.libs.ws._
import play.api.libs.oauth._
import play.api.libs.concurrent._
import play.api.i18n.Messages
import models.User
import views._

object Authentication extends Controller {
    val API_KEY = "ssnl4e8dnzmy"
    val SECRET_KEY = "y8o89xv8tr9WsYA5"
    val AUTHORIZATION_URL = "https://www.linkedin.com/uas/oauth/authorize"
    val ACCESS_TOKEN_URL = "https://api.linkedin.com/uas/oauth/accessToken"
    val REQUEST_TOKEN_URL = "https://api.linkedin.com/uas/oauth/requestToken"
    val SIGN_OUT_URL = "https://api.linkedin.com/uas/oauth/invalidateToken"
    val LINKEDIN_KEY = ConsumerKey(API_KEY, SECRET_KEY)
    val LINKEDIN = OAuth(ServiceInfo(REQUEST_TOKEN_URL, ACCESS_TOKEN_URL, AUTHORIZATION_URL, LINKEDIN_KEY))

    def login = Action { implicit request =>
        Ok(html.linkedin.index())
    }

    def logout = Action {
      Redirect(routes.Application.index).withNewSession
    }

    def authenticate = Action { implicit request =>
        request.queryString.get("oauth_verifier").flatMap(_.headOption).map { verifier =>
            // We got the verifier; now get the access token, store it and back to index
            LINKEDIN.retrieveAccessToken(tokenPair, verifier) match {
                case Right(t) => {
                    // We received the unauthorized tokens in the OAuth object - store it before we proceed
                    Redirect(routes.Application.index).withSession("linkedintoken" -> t.token, "linkedinsecret" -> t.secret)
                }
                case Left(e) => {
                    Logger.error("Error connecting to LinkedIn: " + e.getMessage);
                    Redirect(routes.Authentication.login).flashing("error" -> Messages("linkedin.connect.error.message",e.getMessage))
                }
            }
        }.getOrElse(
            LINKEDIN.retrieveRequestToken("http://localhost:9000/auth") match {
                case Right(t) => {
                    // We received the unauthorized tokens in the OAuth object - store it before we proceed
                    Redirect(LINKEDIN.redirectUrl(t.token)).withSession("linkedintoken" -> t.token, "linkedinsecret" -> t.secret)
                }
                case Left(e) => {
                    Logger.error("Error connecting to LinkedIn: " + e.getMessage);
                    Redirect(routes.Authentication.login).flashing("error" -> Messages("linkedin.connect.error.message",e.getMessage))
                }
            }
        )
    }

  def findTokenPair(implicit request: RequestHeader): Option[RequestToken] = {
    for {
      token <- request.session.get("linkedintoken")
      secret <- request.session.get("linkedinsecret")
    } yield {
      RequestToken(token, secret)
    }
  }
  
  /* This function must be called only inside an Authenticated statement */
  def tokenPair(implicit request: RequestHeader) = findTokenPair.get

  private def fetchCurrentUser(implicit request: RequestHeader): Promise[User] = {
    WS.url("http://api.linkedin.com/v1/people/~")
      .sign(OAuthCalculator(LINKEDIN_KEY, tokenPair))
      .get().extend(_.value match {
        case Redeemed(response) => {
          Logger.debug(response.body)
          User("", "", "", None)
        }
        case Thrown(e) => throw e
      })
  }
}


/*
trait Secured extends Security.Authenticated {
  
  override def username(request: RequestHeader) = Authentification.findTokenPair(request) map (_ => "Logged in")

  override def onUnauthorized(request: RequestHeader) = Redirect(routes.Authentification.login)

i}
*/

