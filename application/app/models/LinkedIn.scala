package models

/**
 * Created by alabbe
 * Date: 24/10/11
 * Time: 11:41
 */

import play.api.libs.json._
import play.api.libs.json.Json._


case class User(id: String, firstName: String, lastName: String, companyId: Option[String] = None)
case class Company(id: Int, name: String)
case class CompanyList(total: Int, values: List[Company]) {
   def first: Option[Company] = values.headOption
}
case class PeopleSearchResult(numResults: Int, people: PeoplePager)
case class PeoplePager(count: Int, start: Int, total: Int, values: List[People])
case class PeopleSkills(total: Int, values: List[People])
case class People(id: String, firstName: String, lastName: String, skills: List[Skill])
case class Skill(id: Int, name: String)

object LinkedinProtocol {
   implicit object UserFormat extends Format[User] {
      def reads(js: JsValue): User = User(
         (js \ "id").as[String],
         (js \ "firstName").as[String],
         (js \ "lastName").as[String],
         (js \ "companyId").asOpt[String])

      def writes(u: User): JsValue = JsObject(List(
         "id" -> JsString(u.id),
         "firstName" -> JsString(u.firstName),
         "companyId" -> u.companyId.map(JsString(_)).getOrElse(JsNull)))
   }

   implicit object CompanyFormat extends Format[Company] {
      def reads(js: JsValue): Company = Company(
         (js \ "id").as[Int],
         (js \ "name").as[String])

      def writes(c: Company): JsValue = JsObject(List(
         "id" -> JsNumber(c.id),
         "name" -> JsString(c.name)))
   }

   implicit object CompanyListFormat extends Format[CompanyList] {
      def reads(js: JsValue): CompanyList = CompanyList(
         (js \ "_total").as[Int],
         (js \ "values").as[List[Company]])

      def writes(cl: CompanyList): JsValue = JsObject(List(
         "_total" -> JsNumber(cl.total),
         "values" -> JsArray(cl.values.map(vs => JsObject(List())))))

   }

   implicit object PeoplePagerFormat extends Format[PeoplePager] {
      def reads(js: JsValue): PeoplePager = PeoplePager(
         (js \ "_count").as[Int],
         (js \ "_start").as[Int],
         (js \ "_total").as[Int],
         (js \ "values").asOpt[List[People]].getOrElse(List()))

      def writes(pp: PeoplePager): JsValue = JsObject(List())
   }

   implicit object PeopleSearchResultFormat extends Format[PeopleSearchResult] {
      def reads(js: JsValue): PeopleSearchResult = PeopleSearchResult(
         (js \ "numResults").as[Int],
         (js \ "people").as[PeoplePager])

      def writes(psr: PeopleSearchResult): JsValue = JsObject(List(
         "numResults" -> JsNumber(psr.numResults),
         "values" -> toJson(psr.people))
      )
   }

   implicit object PeopleSkillsFormat extends Format[PeopleSkills] {
      def reads(js: JsValue): PeopleSkills = PeopleSkills(
         (js \ "_total").as[Int],
         (js \ "values").as[List[People]]
      )

      def writes(ps: PeopleSkills): JsValue = JsObject(List()) //todo
   }

   implicit object PeopleFormat extends Format[People] {
      def reads(js: JsValue): People = People(
         (js \ "id").as[String],
         (js \ "firstName").as[String],
         (js \ "lastName").as[String],
         (js \ "skills" \ "values").asOpt[List[Skill]].getOrElse(Nil)
      )

      def writes(p: People): JsValue = JsObject(List(
         "id" -> JsString(p.id),
         "firstName" -> JsString(p.firstName),
         "lastName" -> JsString(p.lastName)))
   }
   
   implicit object SkillFormat extends Format[Skill] {
      def reads(js:JsValue):Skill = Skill(
         (js \ "id").as[Int],
         (js \ "skill" \ "name").as[String]
      )
      def writes(s:Skill):JsValue = JsObject(List(

      ))
   }

}

/**
 *
 * this is the linkedin json format
 *
 *  {
    "_total": 14,
    "values": [

        {
            "_key": "F0ednDfUa6",
            "firstName": "Earle",
            "id": "F0ednDfUa6",
            "lastName": "C."
        },
        {
            "_key": "NKVog5-Cjt",
            "firstName": "Julien",
            "id": "NKVog5-Cjt",
            "lastName": "R.",
            "skills": {
                "_total": 3,
                "values": [
                    {
                        "id": 7,
                        "skill": {
                            "name": "Scala"
                        }
                    },
                    {
                        "id": 8,
                        "skill": {
                            "name": "HTML5"
                        }
                    },
                    {
                        "id": 9,
                        "skill": {
                            "name": "Java"
                        }
                    }
                ]
            }
        }
    ]
}
 *
 */



