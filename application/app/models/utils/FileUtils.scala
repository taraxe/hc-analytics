package models.utils

import play.api.libs.Files._
import java.io.File
import play.{Application, Play}

/**
 * Created by alabbe
 * Date: 25/10/11
 * Time: 10:53
 */


object FileUtils {
	def printToFile(f: java.io.File)(op: java.io.PrintWriter => Unit) {
		val p = new java.io.PrintWriter(f)
		try {
			op(p)
		} finally {
			p.close()
		}
	}

   def appDir: File = {
      val d = Play.application().getFile("reports")
      if (!d.exists()) d.mkdir()
      d
   }
}