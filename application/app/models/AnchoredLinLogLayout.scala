package models

import linlog._
import java.util._
import math.min
import math.max
import play.Logger
import collection.immutable.{HashSet => HSet, Map => HMap, List => SList}
import collection.mutable.{ListBuffer}
import play.api.libs.json._
import play.api.libs.json.Json._
import akka.actor._
import models.LayoutWorker.{Layout, LinLog}

/**
 * Created by alabbe
 * Date: 22/09/11
 * Time: 04:21
 */


class LayoutWorker extends Actor {
   def receive = {
      case LinLog(f,n) => {
         val r = new AnchoredLinLogLayout(f).execute(n)
         sender ! Layout(toJson(r)(models.AnchoredLinLogLayout.LayoutFormat),r.extractMeta)
      }
   }
}
object LayoutWorker {
   sealed class Compute(filePath:String)
   case class LinLog(filePath:String, cycles:Int=100) extends Compute(filePath)
   case class Layout(json:JsValue,meta:HMap[String,Any])

   val system = ActorSystem("system")
   val ref = system.actorOf(Props[LayoutWorker], name= "layoutWorker")
}


class AnchoredLinLogLayout(val fileName: String) extends models.linlog.LinLogLayout {

	var nodeToPosition: Map[Node, Array[Double]] = new HashMap();
	var nodeToCluster: Map[Node, java.lang.Integer] = new HashMap();
	var edges: List[Edge] = new ArrayList();
	var nodes: List[Node] = new ArrayList();

	def execute(nbIter: Int): AnchoredLinLogLayout = {
		import collection.JavaConversions._

		var graph: Map[String, Map[String, java.lang.Double]] = models.linlog.LinLogLayout.readGraph(fileName)

		val sourceNodes = graph.map((t) => t._1).toSet

		graph = models.linlog.LinLogLayout.makeSymmetricGraph(graph)

		val nameToNode: Map[String, Node] = models.linlog.LinLogLayout.makeNodes(graph)

		nodes = new ArrayList(nameToNode.values)

		edges = models.linlog.LinLogLayout.makeEdges(graph, nameToNode)

		AnchoredLinLogLayout.makeAnchors(nodes, sourceNodes)

		nodeToPosition = AnchoredLinLogLayout.makeInitialPositions(nodes,edges)

		// see class MinimizerBarnesHut for a description of the parameters;
		// for classical "nice" layout (uniformly distributed nodes), use
		//new MinimizerBarnesHut(nodes, edges, -1.0, 2.0, 0.05).minimizeEnergy(nodeToPosition, 100);

		new MinimizerBarnesHut(nodes, edges, 0.0, 3.0, 0.05).minimizeEnergy(nodeToPosition, 100)
		normalize()
		// see class OptimizerModularity for a description of the parameters
		//var nodeToCluster = new OptimizerModularity().execute(nodes, edges, false)
		//todo add modularity to result

		//writePositions(nodeToPosition, nodeToCluster, args(2))
		this
	}

	def extractMeta:HMap[String,Int] ={
		import collection.JavaConversions._
		val parts = nodes.partition(_.isFixed)
		HMap[String,Int](
			("Employees" -> parts._2.size),
			("Skills" -> parts._1.size),
			("Links" -> edges.size())
		)
	}

	def normalize(/*width: Int, height: Int*/): AnchoredLinLogLayout = {

		var minX = Double.MaxValue
		var maxX = -Double.MaxValue
		var minY = Double.MaxValue
		var maxY = -Double.MaxValue
        import collection.JavaConversions._
		for (nodePosition <- nodeToPosition) {
			val pos = nodePosition._2
			minX = min(minX, pos(0))
			maxX = max(maxX, pos(0))
			minY = min(minY, pos(1))
			maxY = max(maxY, pos(1))
		}

		val posScale: Double = min(1 / (maxX - minX), 1 / (maxY - minY))


		nodeToPosition.map {
			ntp =>
				val p = ntp._2
				p(0) = (p(0) - minX) * posScale
				p(1) = (p(1) - minY) * posScale
		}
		this
	}
}
object AnchoredLinLogLayout extends models.linlog.LinLogLayout {

   def initAnchorNodesPosition(anchorNodes: List[Node]): Map[Node, Array[Double]] = {
      val anchorsToPosition = new HashMap[Node, Array[Double]]

      val nbAnchors = anchorNodes.size()

      val factor: Int = 10
      for (i <- 0 until nbAnchors) {
         val pos = Array[Double](factor * math.cos(i * 2 * math.Pi / nbAnchors), factor * math.sin(i * 2 * math.Pi / nbAnchors), 0.0)
         anchorsToPosition.put(anchorNodes.get(i), pos)
      }
      anchorsToPosition
   }

   def makeAnchors(nodes: List[Node], sourceNodes: Set[String]) {
      import collection.JavaConversions._
      nodes.filter(n => !sourceNodes.contains(n.name)).map(_.setFixed(true))
   }

   def makeInitialPositions(nodes: List[Node],edges:List[Edge]): Map[Node, Array[Double]] = {
      import collection.JavaConversions._

      val parts = nodes.partition(_.isFixed)
      val anchors: SList[Node] = (ListBuffer[Node]() ++= parts._1).toList
      val freeNodes = ListBuffer[Node]()
      freeNodes.appendAll(parts._2)

      val nodesToPosition = models.linlog.LinLogLayout.makeInitialPositions(freeNodes, false)
      val freeNodeToAnchor = makeFreeNode2Anchors(edges)

      val orderedAnchors = anchorRingOptimizer(freeNodeToAnchor, anchors)

      val anchorsToPosition = initAnchorNodesPosition(orderedAnchors)

      nodesToPosition.putAll(anchorsToPosition)
      nodesToPosition
   }

   def makeFreeNode2Anchors(edges: Seq[Edge]): HMap[Node, HSet[Node]] = {
      val acc = HMap[Node, HSet[Node]]()

      edges.filter(e => !e.startNode.isFixed && e.endNode.isFixed) // edge from a freenode to an anchor
            .foldLeft(acc)((r, e) => {
         val modifiedEntry = r.get(e.startNode) match {
            case None => HSet(e.endNode)
            case Some(x) => x + e.endNode
         }
         r + (e.startNode -> modifiedEntry)
      })
   }

   def orderFreeNode2Anchors(f2a: HMap[Node, HSet[Node]], orderedAnchors: SList[Node]): HMap[Node, SList[Node]] = {

      f2a.foldLeft(HMap[Node, SList[Node]]())((r, i) => {
         val unsorted = (ListBuffer() ++= i._2).toList
         val sorted = unsorted.sortWith((a, b) => orderedAnchors.indexOf(a) < orderedAnchors.indexOf(b))
         r + (i._1 -> sorted)
      })

   }

   def anchorRingOptimizer(freeNodes2anchors: HMap[Node, HSet[Node]], anchors: SList[Node]): SList[Node] = {

      var freeNodesToOrderedAnchors = orderFreeNode2Anchors(freeNodes2anchors, anchors)
      var p0 = computeGlobalPenalty(freeNodesToOrderedAnchors, anchors)

      var listBuffer = new ListBuffer[Node] ++= anchors

      /*val A = anchors.size
      var d = A / 2

      //todo add the (a) phase


      while (d > 1) {
         var c = false
         do {
            for (i <- 0 until A) {
               Logger.debug("########################### p = " + p0 + " d = " + d);
               var power = 2
               Logger.debug("i :" + i);

               val j = (i + d) % A
               Logger.debug("j :" + j);

               val tmp = listBuffer(j)
               listBuffer(j) = listBuffer(i)
               listBuffer(i) = tmp

               Logger.debug("Switched " + anchors(j).name + " with " + anchors(i).name);

               freeNodesToOrderedAnchors = orderFreeNode2Anchors(freeNodes2anchors, listBuffer.toList)
               val p1 = computeGlobalPenalty(freeNodesToOrderedAnchors, listBuffer.toList)
               Logger.debug("penalty :" + p1);

               if (p1 < p0) {

                  p0 = p1
                  Logger.debug("Ring was optimized, penalty was " + p1 + " new penalty is :" + p0);
                  c = true
               } else {
                  Logger.debug("Ring was not optimized, reverting");
                  val tmp = listBuffer(j)
                  listBuffer(j) = listBuffer(i)
                  listBuffer(i) = tmp
               }
            }
         } while (!c)
         d /= 2
      }*/
      listBuffer.toList
   }

   def computeGlobalPenalty(freeNodeToAnchors: HMap[Node, SList[Node]], allAnchors: SList[Node]): Int = {
      freeNodeToAnchors.foldLeft(0)((r, i) => {
         computePenalty(i._2, 5, allAnchors) match {
            case 0 => r
            case x => r + x
         }
      })
   }

   def computePenalty(connectedAnchors: collection.immutable.List[Node], power: Int, allAnchors: collection.immutable.List[Node]): Int = {
      Logger.debug("Connected anchors " + connectedAnchors.foldLeft("")(_ + " " + _.name));

      val gaps = computeGaps(connectedAnchors, allAnchors)

      val biggestGap = gaps.toList.sortWith(_ > _).head
      val smallerGaps = gaps.filterNot(_ == biggestGap)
      Logger.debug("Gaps " + smallerGaps.foldLeft("")(_ + " " + _ + "^" + power));

      val penalty = smallerGaps.foldLeft(0)((r, i) => r + math.pow(i, power).toInt)
      Logger.debug("= " + penalty);
      penalty
   }

   def computeGaps(connectedAnchors: collection.immutable.List[Node], allAnchors: collection.immutable.List[Node]) = {

      val mod: Int = allAnchors.size

      connectedAnchors.foldLeft(SList[Int]())((r, i) => {
         val pA = allAnchors.indexOf(i)
         val nextAnchorIndex = (connectedAnchors.indexOf(i) + 1) % connectedAnchors.size
         val nextAnchor = connectedAnchors(nextAnchorIndex)
         val pAplus1 = allAnchors.indexOf(nextAnchor)
         (pAplus1 - pA + mod) % mod :: r
      })

   }
   
   implicit object LayoutFormat extends Format[AnchoredLinLogLayout] {
      import collection.JavaConversions._
      def reads(json: JsValue) = null
      def writes(o: AnchoredLinLogLayout) = JsObject(SList(
         "nodes" -> JsArray(o.nodeToPosition.toList.map{ n =>
            JsObject(SList(
               "label" -> JsString(n._1.name),
               "weight" -> JsNumber(n._1.weight),
               "fixed" -> JsBoolean(n._1.isFixed),
               "x" -> JsNumber(n._2.apply(0)),
               "y" -> JsNumber(n._2.apply(1))
            ))
         }),
         "edges" -> JsArray(o.edges.toList.map{ e =>
            JsObject(SList(
               "source" -> JsString(e.startNode.name),
               "target" -> JsString(e.endNode.name),
               "length" -> JsNumber(e.weight)
            ))
         })
      ))
   }
}
