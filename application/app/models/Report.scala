package models

import utils.FileUtils
import java.io.File
import play.Logger
import java.lang.String
import play.api.libs.json._
import util.Random


/**
 * Created by alabbe
 * Date: 22/09/11
 * Time: 04:21
 */
case class Report(id:String, capital:PeopleSkills) {
   
   def write:Report = {
      val file = new File(FileUtils.appDir.getAbsolutePath + File.separator + this.id)
      if (!file.exists()) file.createNewFile;

      FileUtils.printToFile(file)(p => {
         capital.values.foreach{ e =>
            e.skills.foreach{ s =>
               val i: String = e.id + " " + s.id
               Logger.debug("Writing: "+ i)
               p.println(i)
            }
         }
      })
      this
   }
}

object Report {
   def apply(c:PeopleSkills) : Report = {
      new Report(Randomizer.create(),c).write
   }
}

object Randomizer {
   def create(len:Int=6) = {
      java.lang.Long.toHexString(java.lang.Double.doubleToLongBits(scala.math.random))
   }
}